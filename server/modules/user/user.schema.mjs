import Joi from "joi";

const schemaUser = Joi.object({
  first_name: Joi.string().alphanum().min(3).max(30).required(),

  last_name: Joi.string().alphanum().min(3).max(30).required(),

  email: Joi.string().email({
    minDomainSegments: 2,
    tlds: { allow: ["com", "net"] },
  }),

  phone: Joi.string()
    .length(10)
    .pattern(/^[0-9]+$/)
    .required(),
});

export function isValidUser(req) {
  const { error, value } = userSchema.validate(req.body);
  if (error) throw error;
  return value;
}
